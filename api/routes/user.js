const express = require('express');
const router = express.Router()
const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

const User = require('../models/user')
const checkAuth = require('../../middleware/auth')


router.get('/:userId', checkAuth, (req, res, nex) => {
  token = req.body.userData
  const { userId } = req.params
  User.findById({ _id: userId })
    .select('_id email name')
    .exec()
    .then(user => {
      if (user) res.status(200).json({ token, ...user })
      else res.status(404).json({ message: 'User Not Found' })
    })
    .catch(error => res.status(500).json(error))
})

module.exports = router