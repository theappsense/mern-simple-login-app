const express = require('express');
const router = express.Router()
const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

const User = require('../models/user')
// const checkAuth = require('../../middleware/auth')

router.post('/signup', (req, res, next) => {
  const { email, password, name } = req.body

  User.find({ email: email })
    .exec()
    .then(doc => {
      if (doc.length >= 1) {
        return res.status(409)
          .json({ message: "This email already exists, please login" })
      } else {
        bcrypt.hash(password, 10, (err, hash) => {
          if (err) {
            return res.status(500)
              .json({ error: err })
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: email, password: hash, name: name
            })
            user.save()
              .then(() => res.status(201).json({
                message: "Signup successful",
                status: 200
              }))
              .catch(error => res.status(500).json(error))
          }
        })
      }
    })
    .catch(error => res.status(500).json({ message: "An error occured, please try again", error }))
  // res.status(200).json({ message: "Signup successfull" })

})

router.post('/login', (req, res, next) => {
  const { email, password } = req.body

  User.find({ email: email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({ message: 'Login was not successful. Email does not exist' })
      }
      else {
        bcrypt.compare(password, user[0].password, (error, response) => {
          if (error) {
            return res.status(401).json({ message: 'Login was not successful. Password is invalid' })
          }
          if (response) {
            const token = jwt.sign({
              email: user[0].email,
              password: user[0].password
            },
              'UserSecret',
              { expiresIn: '1hr' }
            )

            return res.status(200).json({
              status: 200,
              token: token,
              user: {
                name: user[0].name,
                email: user[0].email,
                message: 'Login is successful',
                _id: user[0]._id,
                request: {
                  type: 'GET, PATCH, DELETE',
                  url: `/api/v1/user/${user[0]._id}`
                },
              }


            })
          }
        })
      }
    })
    .catch(error => res.status(500).json({ message: "An error occured, please try again", error }))
})

module.exports = router