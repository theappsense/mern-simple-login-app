const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const authRoutes = require('./api/routes/auth')
const userRoutes = require('./api/routes/user')


mongoose.connect('mongodb://ambassEugene:Mlab6588002580!@ds253017.mlab.com:53017/mern-demo', {
  useNewUrlParser: true
})
mongoose.set('useCreateIndex', true)


const app = express();

// console.log(userRoutes)


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-Type, Accept, Authorization')

  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
    return res.status(200).json({})
  }
  next()
})

app.use('/api/v1/auth', authRoutes)
app.use('/api/v1/user', userRoutes)


app.use((req, res, next) => {
  const error = new Error('Resource is not available for now')
  error.status = 404
  next(error)
})

app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: { message: error.message }
  })
})


module.exports = app